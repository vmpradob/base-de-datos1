
-- ----------------------------
-- Table structure for Cargos
-- ----------------------------
DROP TABLE IF EXISTS "Cargos";
CREATE TABLE "Cargos" (
"id_consejo"    serial  NOT NULL,
"nombre"        text    NOT NULL
);

ALTER TABLE "Cargos" ADD PRIMARY KEY ("id_consejo","nombre"),
                     ADD UNIQUE ("id_consejo", "nombre"),
                     ADD FOREIGN KEY ("id_consejo") REFERENCES "Consejos" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;;


-- ----------------------------
-- Table structure for Carreras
-- ----------------------------
DROP TABLE IF EXISTS "Carreras";
CREATE TABLE "Carreras" (
"id"     serial UNIQUE NOT NULL,
"nombre" text          NOT NULL
);
ALTER TABLE "Carreras" ADD PRIMARY KEY ("id")


-- ----------------------------
-- Table structure for Consejos
-- ----------------------------
DROP TABLE IF EXISTS "Consejos";
CREATE TABLE "Consejos" (
"id"     serial UNIQUE  NOT NULL,
"nombre" text           NOT NULL
);
ALTER TABLE "Consejos" ADD PRIMARY KEY("id");

-- ----------------------------
-- Table structure for Egresados
-- ----------------------------
DROP TABLE IF EXISTS "Egresados";
CREATE TABLE "Egresados" (
"id"            serial      UNIQUE NOT NULL,
"cedula"        integer     UNIQUE NOT NULL,
"nombre"        varchar(50)        NOT NULL,
"apellido"      varchar(50)        NOT NULL,
"correo"        varchar(50) UNIQUE NOT NULL,
"direccion"     varchar(50),
"fec_egreso"    date               NOT NULL,
"contraseña"    varchar(50)        NOT NULL
);
ALTER TABLE "Egresados" ADD PRIMARY KEY("id"),

-- ----------------------------
-- Table structure for Extensiones
-- ----------------------------
DROP TABLE IF EXISTS "Extensiones";
CREATE TABLE "Extensiones" (
"rif"       varchar(12) NOT NULL,
"ubicacion" text        NOT NULL
);
Alter TABLE "Extensiones" ADD PRIMARY KEY("rif");

-- ----------------------------
-- Table structure for Periodos_electorales
-- ----------------------------
DROP TABLE IF EXISTS "Periodos_electorales";
CREATE TABLE "Periodos_electorales" (
"id"                serial  NOT NULL,
"fec_ini"           date    NOT NULL,
"fec_fin"           date    NOT NULL,
"fec_votacion"      date    NOT NULL,
"fec_postulacion"   date    NOT NULL
);
ALTER TABLE "Periodos_electorales" ADD PRIMARY KEY("id");
-- ----------------------------
-- Table structure for Egresados_x_Carreras
-- ----------------------------
Create Table "Egresados_x_Carreras" (
    id_egresado integer not null,
    id_carrera  integer not null,
    PRIMARY KEY (id_egresado,id_carrera)
);

ALTER TABLE "Egresados_x_Carreras"  ADD FOREIGN KEY ("id_egresado") REFERENCES "Egresados" ("id") ON DELETE NO ACTION ON UPDATE CASCADE,
                                    ADD FOREIGN KEY ("id_carrera") REFERENCES "Carreras" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;
