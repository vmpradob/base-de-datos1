<?php
require('app/Carrera.php');

use App\Carrera;
$estudiantes = \App\Carrera::scopeNombre('');
?>

<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GaleriaPP</title>
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="node_modules/datatables/media/css/jquery.dataTables.css">
</head>
<body>
    <table id="tablaEgresados" class="table dataTable" cellspacing="0" width="100%">
        <thead></thead>
        <tbody>
            <?php
                foreach ($estudiantes as $estudiante) {
                    $string =
                        '<tr>
                            <td>' . $estudiante->nombre . '</td>
                        </tr>';
                }
                    echo $string;
            ?>
        </tbody>
        <tfoot></tfoot>
    </table>

</body>
<script src="node_modules/jquery/dist/jquery.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
<script src="node_modules/datatables/media/js/jquery.dataTables.js"></script>

<script>
    $(document).ready(function (){
        tabla = $('#tablaEgresados').DataTable({
            columns: [
                {
                    title: "Nombre"
                }
            ]

        });
    })
</script>
</html>