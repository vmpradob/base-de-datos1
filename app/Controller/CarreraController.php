<?php
/**
 * Created by PhpStorm.
 * User: VMPB
 * Date: 26/8/2017
 * Time: 8:33 AM
 */

namespace App;
use DatabasePDO;
use PDO;
use PDOException;

require_once('App/ConsultaDB/databasePDO.php');

class CarreraController
{
    //done
    public static function index()
    {
        $consulta = 'SELECT * FROM public."Carreras" ORDER BY id ASC';
        // Preparar sentencia
        try {
            $comando = DatabasePDO::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();
            return $comando->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return $e->errorInfo;
        }
    }
    //done
    public static function show($id)
    {
        // Consulta de la tabla usuarios
        $consulta = 'SELECT * FROM  public."Carreras" WHERE  public."Carreras"."id" =?';
        try {
            // Preparar sentencia
            $comando = DatabasePDO::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute(array($id));
            // Capturar primera fila del resultado
            $row = $comando->fetch(PDO::FETCH_ASSOC);
            return $row;
        } catch (PDOException $e) {
            // Aquí puedes clasificar el error dependiendo de la excepción
            // para presentarlo en la respuesta Json
            return $e->errorInfo;
        }
    }

    public static function store($array)
    {
        try{
            $comando = 'INSERT INTO public."Carreras" (nombre) VALUES (?)';

            $sentencia = DatabasePDO::getInstance()->getDb()->prepare($comando);
            return $sentencia->execute([$array['nombre']]);
        } catch (PDOException $e){
            return $e->errorInfo;
        }

    }
    public static function delete($id){
        $comando='DELETE from public."Carreras" WHERE id=?';
        $sentencia=  DatabasePDO::getInstance()->getDb()->prepare($comando);
        return $sentencia->execute(array($id));
    }
    //TODO: probar el update
    public static function update($id, $array)
    {
        $aux =0;
        $datos = [];
        if($show =self::show($id)){
            $set = null;

            if(array_key_exists('nombre',$array)){
                $set = $set.', nombre ?';
                $datos[$aux++] = $array['nombre'];
            }
            $datos[$aux] = $id;
            $set = substr($set,1);
            $comando = 'UPDATE public."Carreras" SET'. $set . ' WHERE id= ?';

            $sentencia =  DatabasePDO::getInstance()->getDb()->prepare($comando);

            return $sentencia->execute($datos);
        }else
            return $show;
    }

    public static function scope($campo, $busqueda)
    {
        $busqueda = '%'.$busqueda.'%';
        // Consulta de la tabla usuarios
        $consulta = 'SELECT * FROM  public."Carreras" WHERE  public."Carreras"'.$campo." LIKE ?";
        //return $consulta;
        try {
            $comando = DatabasePDO::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute([$busqueda]);
            return $comando->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            // Aquí puedes clasificar el error dependiendo de la excepción
            // para presentarlo en la respuesta Json
            return $e->errorInfo;
        }
    }
    //TODO: probar
    public static function getEstudiantes($id)
    {
        if($show =self::show($id)){
            $comando = 'select * from public.""Carreras""  WHERE "Carreras".id= ? and "Carreras"s_x_egresados.id_est = egresados.id and "Carreras"_x_egresados.id_carrerra = "Carreras".id';

            $sentencia =  DatabasePDO::getInstance()->getDb()->prepare($comando);

            return $sentencia->execute($id);
        }else
            return $show;
    }


}