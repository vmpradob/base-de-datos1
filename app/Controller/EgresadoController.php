<?php
/**
 * Created by PhpStorm.
 * User: VMPB
 * Date: 26/8/2017
 * Time: 8:33 AM
 */

namespace App;
use DatabasePDO;
use PDO;
use PDOException;

require_once('App/ConsultaDB/databasePDO.php');


class EgresadoController
{
    //done
    public static function index()
    {
        $consulta = 'SELECT * FROM public.Egresados ORDER BY id ASC';
        // Preparar sentencia
        try {
            $comando = DatabasePDO::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();
            return $comando->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return $e->errorInfo;
        }
    }
    //done
    public static function show($id)
    {
        // Consulta de la tabla usuarios
        $consulta = 'SELECT * FROM  public.Egresados WHERE  public.Egresados."id" =?';
        try {
            // Preparar sentencia
            $comando = DatabasePDO::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute(array($id));
            // Capturar primera fila del resultado
            $row = $comando->fetch(PDO::FETCH_ASSOC);
            return $row;
        } catch (PDOException $e) {
            // Aquí puedes clasificar el error dependiendo de la excepción
            // para presentarlo en la respuesta Json
            return $e->errorInfo;
        }
    }

    public static function store($array)
    {
        try{
            $comando = 'INSERT INTO public.Egresados (nombre, apellido,cedula,direccion,fec_egreso,correo) VALUES (?,?,?,?,?,?)';

            $sentencia = DatabasePDO::getInstance()->getDb()->prepare($comando);
            return $sentencia->execute([$array['nombre'],$array['apellido'],$array['cedula'],$array['direccion'],$array['fec_egreso'],$array['correo']]);
        } catch (PDOException $e){
            return $e->errorInfo;
        }

    }
    public static function delete($id){
        $comando='DELETE from public.Egresados WHERE id=?';
        $sentencia=  DatabasePDO::getInstance()->getDb()->prepare($comando);
        return $sentencia->execute(array($id));
    }
    //TODO: probar el update
    public static function update($id, $array)
    {
        $aux =0;
        $datos = [];
        if($show =self::show($id)){
            $set = null;

            if(array_key_exists('nombre',$array)){
                $set = $set.', nombre ?';
                $datos[$aux++] = $array['nombre'];
            }
            if(array_key_exists('apellido',$array)){
                $set = $set.', apellido ?';
                $datos[$aux++] = $array['apellido'];
            }
            if(array_key_exists('direccion',$array)){
                $set = $set.', direccion ?';
                $datos[$aux++] = $array['direccion'];
            }
            if(array_key_exists('cedula',$array)){
                $set = $set.', cedula ?';
                $datos[$aux++] = $array['cedula'];
            }
            if(array_key_exists('fec_egreso',$array)){
                $set = $set.',fec_egreso ?';
                $datos[$aux++] = $array['nacionalidad'];
            }
            $datos[$aux] = $id;
            $set = substr($set,1);
            $comando = 'UPDATE public."Egresados" SET'. $set . ' WHERE $id= ?';

            $sentencia =  DatabasePDO::getInstance()->getDb()->prepare($comando);

            return $sentencia->execute($datos);
        }else
            return $show;
    }

    public static function scope($campo, $busqueda)
    {
        $busqueda = '%'.$busqueda.'%';
        // Consulta de la tabla usuarios
        $consulta = "SELECT * FROM  estudiantes WHERE  estudiantes.".$campo." LIKE ?";
        //return $consulta;
        try {
            $comando = DatabasePDO::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute([$busqueda]);
            return $comando->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            // Aquí puedes clasificar el error dependiendo de la excepción
            // para presentarlo en la respuesta Json
            return $e->errorInfo;
        }
    }
    //TODO: probar
    public static function getCarreras($id_egresado)
    {
        if($show =self::show($id_egresado)){
            $set = null;
            $comando = 'select * from public.egresados, public.carrera, public.carreras_x_estudiantes 
                        WHERE public.egresados.id =? 
                        and public.egresados.id = public.carreras_x_carreras.id_est
                        and public.carreras_x_carreras.id_carrrera = public.carrera.id';
            $sentencia =  DatabasePDO::getInstance()->getDb()->prepare($comando);
            return $sentencia->execute($id_egresado);
        }else
            return $show;
    }


}