<?php
/**
 * Created by PhpStorm.
 * User: VMPB
 * Date: 26/8/2017
 * Time: 8:25 AM
 */

namespace App;
require ('App/Controller/EgresadoController.php ');


class Egresado
{
    protected $id;
    public    $nombre;
    public    $apellido;
    public    $cedula;
    public    $direccion;
    public    $fec_egreso;
    public    $correo;

    function __construct(array $array)
    {
        if(array_key_exists('id',$array)) $this->setId($array['id']);
        if(array_key_exists('nombre',$array)) $this->setNombre($array['nombre']);
        if(array_key_exists('apellido',$array)) $this->setApellido($array['apellido']);
        if(array_key_exists('cedula',$array)) $this->setCedula($array['cedula']);
        if(array_key_exists('correo',$array)) $this->setCorreo($array['correo']);
        if(array_key_exists('direccion',$array)) $this->setDireccion($array['direccion']);
        if(array_key_exists('fec_egreso',$array)) $this->setFecEgreso($array['fec_egreso']);
    }

    public function getId()
    {
        return $this->id;
    }
    private function setId($id)
    {
        $this->id = $id;
    }
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    }
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }
    public function setCedula($cedula)
    {
        $this->cedula = $cedula;
    }
    public function setCorreo($correo)
    {
        $this->correo = $correo;
    }
    public function setFecEgreso($fec_egreso)
    {
        $this->fec_egreso = $fec_egreso;
    }



    //methods
    public static function index()
    {
        $usuarios = null;
        $resultado = EgresadoController::index();
        for ($i=0; $i < count($resultado);$i++)
        {
            $usuarios[$i] = new Egresado($resultado[$i]);
        }
        return $usuarios;
    }
    public static function show($id)
    {
        return new Egresado(EgresadoController::show($id));
    }
    public static function store( $array){
        return EgresadoController::store($array);
    }
    public static function destroy($id)
    {
        return EgresadoController::delete($id);
    }
    public static function update($id,$datos)
    {
        return EgresadoController::update($id,$datos);
    }

    public static function scopeNombre($nombre)
    {
        $result = EgresadoController::scope('nombre',$nombre);
        for($i=0; $i< count($result); $i++){
            $estudiantes[$i] = new Egresado($result[$i]);
        }
        return $estudiantes;
    }
    public static function scopeApellido($apellido)
    {
        $estudiantes =null;
        $result = EgresadoController::scope('apellido',$apellido);
        for($i=0; $i< count($result); $i++){
            $estudiantes[$i] = new Egresado($result[$i]);
        }
        return $estudiantes;
    }
    public static function scopeSexo($sexo)
    {
        $estudiantes = null;
        $result = EgresadoController::scope('sexo',$sexo);
        for($i=0; $i< count($result); $i++){
            $estudiantes[$i] = new Egresado($result[$i]);
        }
        return $estudiantes;
    }
    public static function scopeNacionalidad($nacionalidad)
    {
        $result = EgresadoController::scope('nacionalidad',$nacionalidad);
        for($i=0; $i< count($result); $i++){
            $estudiantes[$i] = new Egresado($result[$i]);
        }
        return $estudiantes;
    }

    public static function getCarreras($id_egresado){

        $result = EgresadoController::getCarreras($id_egresado);

        for($i=0; $i< count($result); $i++){
            $egresado[$i] = new Egresado($result[$i]);
        }
        return $egresado;
    }


}