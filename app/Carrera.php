<?php
/**
 * Created by PhpStorm.
 * User: VMPB
 * Date: 26/8/2017
 * Time: 8:17 AM
 */

namespace App;
require('App/Controller/CarreraController.php');

class Carrera
{
    protected $id;
    public $nombre;


    public function __construct(array $array)
    {
        if(array_key_exists('id',$array)) $this->setId($array['id']);
        if(array_key_exists('nombre',$array)) $this->setNombre($array['nombre']);
    }

    public function getId()
    {
        return $this->id;
    }


    public function setId($id)
    {
        $this->id = $id;
    }
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public static function index()
    {
        $carreras = null;
        $resultado = CarreraController::index();
        for ($i=0; $i < count($resultado);$i++)
        {
            $carreras[$i] = new Carrera($resultado[$i]);
        }
        return $carreras;
    }
    public static function show($id)
    {
        return new Carrera(CarreraController::show($id));
    }
    public static function store( $array){
        return CarreraController::store($array);
    }
    public static function destroy($id)
    {
        return CarreraController::delete($id);
    }
    //TODO: probar update
    public static function update($id,$datos)
    {
        return CarreraController::update($id,$datos);
    }

    public static function scopeNombre($nombre){
        $result = CarreraController::scope('nombre',$nombre);
        for($i=0; $i< count($result); $i++){
            $carreras[$i] = new Carrera($result[$i]);
        }
        return $carreras;
    }

    public static function Estudiantes($carrera_id)
    {
        $result = CarreraController::getEstudiantes($carrera_id);
        for($i=0; $i< count($result); $i++){
            $estudiantes[$i] = new Egresado($result[$i]);
        }
        return $estudiantes;
    }

}